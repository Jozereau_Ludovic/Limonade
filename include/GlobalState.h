#pragma once

#include <memory>
#include <array>
#include <vector>

#include <glk/InPlace.h>
#include <glk/gl/Vbo.h>
#include <glk/gl/InstanceQueue.h>
#include <glk/gl/Texture.h>
#include <glk/TaggedContainer.h>

#include "Programs.h"
#include "Input.h"
#include "ParticulePop.h"
#include "Text.h"
#include "Attribs.h"

extern glk::TaggedContainer gContainer;

template <class Tag, class... Args>
auto &global(Args &&... args) {
	return gContainer.get<Tag>(std::forward<Args>(args)...);
}

struct GlobalState {
	static constexpr int W_WIDTH = 1024, W_HEIGHT = 640;
	static constexpr float PX_PER_METER = 32.0f, METERS_PER_PX = 1.0f / PX_PER_METER;
	static constexpr float M_WIDTH = W_WIDTH * METERS_PER_PX, M_HEIGHT = W_HEIGHT * METERS_PER_PX;

	GlobalState();
	~GlobalState();

	void update();
	void display();

	int t;

	std::array<float, 2> scores;
	std::array<Text, 3> scoreTxt;

	std::array<std::unique_ptr<struct Perso>, 2> persos;
	std::vector<std::unique_ptr<struct Bulle>> bubulles;
	std::vector<std::unique_ptr<struct Bulle>> newBubulles;

	std::vector<ParticulePop> particles;
};

#define TAG(Tag, ...)\
        struct Tag { using type = __VA_ARGS__; }

TAG(state, GlobalState);

TAG(input, Input);
TAG(unitVbo, glk::gl::Vbo<VertexCoord>);
TAG(progs, Programs);

TAG(fontTex, glk::gl::TextureName);

namespace dq {
	TAG(bg, glk::gl::InstanceQueue<VertexCoord, NoAttrib>);
	TAG(bulles, glk::gl::InstanceQueue<VertexCoord, BulleAttribs>);
	TAG(perso, glk::gl::InstanceQueue<VertexCoord, PersoAttribs>);
	TAG(parts, glk::gl::InstanceQueue<VertexCoord, ParticuleAttribs>);
}

TAG(camera, glm::mat3);

#undef TAG
