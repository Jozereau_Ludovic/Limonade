#pragma once

#include <glm/vec2.hpp>

#include <glk/gl/VertexAttribs.h>
#include <glk/gl/InstanceQueue.h>

struct VertexCoord;

GLK_GL_ATTRIB_STRUCT(TextAttribs,
	(GLuint, index)
);

struct Text {
	Text(std::string const &str = "");

	void setText(std::string const &str);

	void display();

	glm::mat3 transform() const;

	glm::vec2 pos{0.0f};
	glm::vec2 alignment{0.0f};
	glm::vec2 scale{1.0f};
	glm::vec4 color{1.0f};

private:
	glk::gl::InstanceQueue<VertexCoord, TextAttribs> _queue;
	std::size_t _length;
};
