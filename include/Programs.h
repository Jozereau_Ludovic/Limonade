#pragma once

#include <glk/path.h>
#include <glk/gl/Program.h>

struct Programs {
	GLK_GL_PROGRAM(Bulles,
		(glm::mat3, pvMatrix)
	) bulles{
		glk::gl::VertShader{"data/bulle.vert"_f},
		glk::gl::FragShader{"data/bulle.frag"_f}
	};

	GLK_GL_PROGRAM(Perso,
		(glm::mat3, pvMatrix)
	) perso{
		glk::gl::VertShader{"data/perso.vert"_f},
		glk::gl::FragShader{"data/perso.frag"_f}
	};

	GLK_GL_PROGRAM(Background,
		(float, t)
	) background{
		glk::gl::VertShader{"data/bg.vert"_f},
		glk::gl::FragShader{"data/bg.frag"_f}
	};

	GLK_GL_PROGRAM(Particles,
		(glm::mat3, pvMatrix)
	) particles{
		glk::gl::VertShader{"data/part.vert"_f},
		glk::gl::FragShader{"data/part.frag"_f}
	};

	GLK_GL_PROGRAM(Text,
		(glm::mat3, pvMatrix)
		(glk::gl::Sampler2d, fontAtlas)
		(glm::vec4, color)
	) text{
		glk::gl::VertShader{"data/text.vert"_f},
		glk::gl::FragShader{"data/text.frag"_f}
	};
};
