#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

struct ParticuleAttribs;

struct ParticulePop {

	ParticulePop(glm::vec2 pos, glm::vec2 vel, glm::vec3 col);

	void update();

	ParticuleAttribs makeAttribs() const;

	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec3 col;
};
