#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

struct BulleAttribs;

struct Bulle {

	Bulle(glm::vec2 pos, glm::vec2 vel, float radius);

	void update();
	void pop();

	BulleAttribs makeAttribs() const;

	glm::vec2 pos;
	glm::vec2 vel;
	float radius;
	glm::vec3 col;
	int time = 30.0f * 7.5f/radius, invinc = 30;
	bool isAlive = true;
};
