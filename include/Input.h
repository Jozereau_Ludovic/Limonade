#pragma once

#include <array>
#include <cstdint>

struct Input {
	Input();

	void update();

	bool goLeft(int player) const;
	bool goRight(int player) const;
	bool jump(int player) const;
	bool jumpPressed(int player) const;

private:
	std::uint8_t const *_keys;
	std::array<bool, 2> pJump;
};
