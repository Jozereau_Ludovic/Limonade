#pragma once

#include <glk/gl/VertexAttribs.h>

//fmtoff
GLK_GL_ATTRIB_STRUCT(
	VertexCoord,
	(glm::vec2, vertexCoord)
);

GLK_GL_ATTRIB_STRUCT(
	BulleAttribs,
	(glm::vec2, pos)
	(glm::vec2, size)
	(float, ttl)
	(glm::vec3, color)
);

GLK_GL_ATTRIB_STRUCT(
	PersoAttribs,
	(glm::mat3, transform)
	(int, pNum)
);

GLK_GL_ATTRIB_STRUCT(
	ParticuleAttribs,
	(glm::vec2, pos)
	(glm::vec3, color)
);

GLK_GL_ATTRIB_STRUCT(
	NoAttrib,
);
//fmton
