#pragma once

#include <glm/glm.hpp>

#include "Bulle.h"

struct PersoAttribs;

struct Perso {

	Perso(int num);

	void abullir(Bulle &bulle);
    void avancer(int sens);
    void sauter();
    void update();

	void cling();

	void bubblePopped(Bulle const &b);

	PersoAttribs makeAttribs() const;

    glm::vec2 pos;
    glm::vec2 vel;
    glm::vec2 vert{0.0f, -1.0f};
	float jmp = 1.0f;
	int num;
	Bulle *bulle = nullptr;
};
