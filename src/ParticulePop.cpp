#include "ParticulePop.h"

#include "GlobalState.h"

namespace {
	constexpr float GRAVITY = 0.005f;
}


ParticulePop::ParticulePop(glm::vec2 pos, glm::vec2 vel, glm::vec3 col)
: pos{pos}, vel{vel}, col{col} { }

void ParticulePop::update() {
	vel.y += GRAVITY;
	pos += vel;
}

ParticuleAttribs ParticulePop::makeAttribs() const {
	return {pos, col};
}
