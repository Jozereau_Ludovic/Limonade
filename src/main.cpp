#include <iostream>

#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_events.h>

#include <GL/glew.h>

#include <glk/gl/util.h>

#include "GlobalState.h"
#include "Bulle.h"

int main(int, char **) {

	std::ios::sync_with_stdio(false);

	auto sdlWindow = SDL_CreateWindow(
		"Limonade",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		GlobalState::W_WIDTH,
		GlobalState::W_HEIGHT,
		SDL_WINDOW_OPENGL
	);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	auto glContext = SDL_GL_CreateContext(sdlWindow);
	assert(glContext);
	SDL_GL_MakeCurrent(sdlWindow, glContext);

	SDL_ShowWindow(sdlWindow);
	SDL_ShowCursor(false);

	glewExperimental = true;
	GLenum glewStatus =glewInit();
	if(glewStatus != GLEW_OK) {
		std::cout << "Could not initialize GLEW, error : " << glewGetErrorString(glewStatus) << '\n';
		return 1;
	}

	(void)glGetError();

	{
		int maj, min;
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &maj);
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &min);
		std::cout << "Initialized OpenGL " << maj << "." << min << '\n';
	}

	// For 1 byte-per-pixel textures
	TRY_GL(glPixelStorei(GL_PACK_ALIGNMENT, 1));
	TRY_GL(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	TRY_GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

	GLint maxTextureUnits;
	TRY_GL(glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextureUnits));

	GLuint sampler;
	TRY_GL(glGenSamplers(1, &sampler));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST));

	for(int i = 0u; i < maxTextureUnits; ++i) {
		TRY_GL(glActiveTexture(GL_TEXTURE0 + i));
		TRY_GL(glBindSampler(i, sampler));
	}

	gContainer.construct<state>();

	std::uint32_t timer = SDL_GetTicks();
	SDL_Event ev;

	bool loop = true;
	do {
		global<input>().update();
		while(SDL_PollEvent(&ev)) {
			if(ev.type == SDL_QUIT
				|| (ev.type == SDL_KEYDOWN && ev.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
				loop = false;
		}

		TRY_GL(glClear(GL_COLOR_BUFFER_BIT));
		TRY_GL(glEnable(GL_BLEND));
		TRY_GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		global<state>().update();
		global<state>().display();

		SDL_GL_SwapWindow(sdlWindow);

		std::uint32_t frameTime = SDL_GetTicks() - timer;
		if(frameTime < 16u)
			SDL_Delay(16u - frameTime);
		timer += frameTime;
	} while(loop);

	gContainer.clear();

	TRY_GL(glDeleteSamplers(1, &sampler));

	return 0;
}
