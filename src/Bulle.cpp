#include "Bulle.h"

#include <random>
#include <ctime>
#include <cmath>

#include <glm/glm.hpp>

#include "GlobalState.h"

std::mt19937 prng(std::time(nullptr));

namespace {
	constexpr float
		MAX_VEL = 0.02f,
		FRICTION = 0.95f,
		PART_SPEED = 0.1f,
		MIN_SPLIT_RADIUS = 0.5f;
	constexpr int POP_PARTS = 20;

	using dist = std::uniform_real_distribution<float>;
}


Bulle::Bulle(glm::vec2 pos, glm::vec2 vel, float radius)
: pos{pos}, vel{vel}, radius{radius}, col{
	dist{0.3f, 1.0f}(prng),
	dist{0.3f, 1.0f}(prng),
	dist{0.3f, 1.0f}(prng)
} { }

void Bulle::update() {
	if(glm::dot(vel, vel) > glk::sqr(MAX_VEL))
		vel *= FRICTION;

	pos += vel;

	if(vel.x > 0.0f && pos.x > GlobalState::M_WIDTH + radius)
		pos.x = -radius;
	if(vel.x < 0.0f && pos.x < -radius)
		pos.x = GlobalState::M_WIDTH + radius;
	if(vel.y > 0.0f && pos.y > GlobalState::M_HEIGHT + radius)
		pos.y = -radius;
	if(vel.y < 0.0f && pos.y < -radius)
		pos.y = GlobalState::M_HEIGHT + radius;

	if(invinc > 0)
		--invinc;
}

void Bulle::pop() {
	if(invinc > 0)
		return;

	isAlive = false;
	for(int i = 0; i < POP_PARTS; ++i) {
		float a = i * 2.0f * float(M_PI) / POP_PARTS;
		glm::vec2 r(std::cos(a), std::sin(a));
		global<state>().particles.emplace_back(
			pos + radius * r,
			PART_SPEED * dist{0.5f, 1.5f}(prng) * r,
			col
		);
	}

	if(radius > MIN_SPLIT_RADIUS) {
		int nBulles = std::uniform_int_distribution<int>{
			2,
			std::max(2, int(4 * radius))
		}(prng);
		for (int i = 0; i < nBulles; ++i)
			global<state>().newBubulles.emplace_back(std::make_unique<Bulle>(
				pos + glm::vec2(dist{-0.2f, 0.2f}(prng), dist{-0.2f, 0.2f}(prng)),
				glm::vec2(0.0f),
				radius / nBulles
			));
	}
}

BulleAttribs Bulle::makeAttribs() const {
	return {pos, glm::vec2(2.0f * radius), 1.0f, col};
}