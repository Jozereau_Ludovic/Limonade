#include "GlobalState.h"

#include <random>
#include <ctime>
#include <algorithm>
#include <iterator>
#include <iomanip>

#include "Bulle.h"
#include "Perso.h"

glk::TaggedContainer gContainer;

namespace {
	std::array<VertexCoord, 4> const unitQuad{{
		{{0.0f, 0.0f}},
		{{1.0f, 0.0f}},
		{{0.0f, 1.0f}},
		{{1.0f, 1.0f}}
	}};
}

GlobalState::GlobalState()
: t{(
	gContainer.construct<input>(),
	gContainer.construct<unitVbo>(glk::gl::VboUsage::staticDraw, begin(unitQuad), end(unitQuad)),
	gContainer.construct<progs>(),
	gContainer.construct<fontTex>(glk::gl::loadTextureFromFile("data/font.png")),
	gContainer.construct<dq::bg>(global<unitVbo>(), global<progs>().background),
	gContainer.construct<dq::bulles>(global<unitVbo>(), global<progs>().bulles),
	gContainer.construct<dq::perso>(global<unitVbo>(), global<progs>().perso),
	gContainer.construct<dq::parts>(global<unitVbo>(), global<progs>().particles),
	gContainer.construct<camera>(
		2.0f / M_WIDTH, 0.0f, 0.0f,
		0.0f, -2.0f / M_HEIGHT, 0.0f,
		-1.0f, 1.0f, 1.0f
	),
	0
)}
, scores{{0, 0}}
, scoreTxt{{
	{"0.00"},
	{"-"},
	{"0.00"}
}}
, persos{{std::make_unique<Perso>(0), std::make_unique<Perso>(1)}} {

	scoreTxt[0].scale = glm::vec2(2.0f);
	scoreTxt[0].color = glm::vec4(0.1f, 0.5f, 1.0f, 1.0f);
	scoreTxt[0].alignment = glm::vec2(1.0f, 1.0f);
	scoreTxt[0].pos = glm::vec2(M_WIDTH / 2.0f - 1.0f, M_HEIGHT - 1.5f);

	scoreTxt[1].scale = glm::vec2(2.0f);
	scoreTxt[1].alignment = glm::vec2(0.5f, 1.0f);
	scoreTxt[1].pos = glm::vec2(M_WIDTH / 2.0f, M_HEIGHT - 1.5f);

	scoreTxt[2].scale = glm::vec2(2.0f);
	scoreTxt[2].color = glm::vec4(1.0f, 0.5f, 0.1f, 1.0f);
	scoreTxt[2].alignment = glm::vec2(0.0f, 1.0f);
	scoreTxt[2].pos = glm::vec2(M_WIDTH / 2.0f + 1.0f, M_HEIGHT - 1.5f);

	auto &prog = global<progs>();
	auto &cam = global<camera>();

	prog.bulles.use();
	prog.bulles.pvMatrix = cam;

	prog.perso.use();
	prog.perso.pvMatrix = cam;

	prog.particles.use();
	prog.particles.pvMatrix = cam;

	global<dq::bg>().enqueue({});

	std::mt19937 prng(std::time(nullptr));
	using dist = std::uniform_real_distribution<float>;

	for(int i = 0; i < 8; ++i) {
		auto b = std::make_unique<Bulle>(
			glm::vec2(
				dist{0.0f, M_WIDTH}(prng),
				dist{0.0f, M_HEIGHT}(prng)
			),
			glm::vec2(
				dist{-0.01f, 0.01f}(prng),
				dist{-0.01f, 0.01f}(prng)
			),
			dist{0.5, 2.0f}(prng)
		);
		bubulles.emplace_back(std::move(b));
	}

	persos[0]->pos = glm::vec2(M_WIDTH / 2.0f, M_HEIGHT / 2.0f);
	persos[0]->abullir(*bubulles[0]);

	persos[1]->pos = glm::vec2(M_WIDTH / 2.0f, M_HEIGHT / 2.0f);
	persos[1]->abullir(*bubulles[1]);
}

GlobalState::~GlobalState() = default;

constexpr float FORCE = 0.001f;

void pushBubulles(Bulle &b1, Bulle &b2) {
	glm::vec2 dist = b2.pos - b1.pos;
	if(glk::sqr(b1.radius + b2.radius + 0.1f) > glm::dot(dist, dist)) {
		float w1 = b1.radius * b1.radius * b1.radius;
		float w2 = b2.radius * b2.radius * b2.radius;
		dist = FORCE * glm::normalize(dist);
		b1.vel -= dist * (w1 + w2) / w1;
		b2.vel += dist * (w1 + w2) / w2;
	}
}

void GlobalState::update() {
	++t;

	for(auto i = begin(bubulles), e = end(bubulles); i != e; ++i)
		for(auto j = i + 1u; j != e; ++j)
			pushBubulles(**i, **j);

	for(auto const &b : bubulles)
		b->update();

	for(auto const &p : persos)
		p->update();

	auto firstDead = std::partition(
		begin(bubulles),
		end(bubulles),
		[](auto const &p) {
			return p->isAlive;
		}
	);

	for(auto const &p : persos)
		for(auto i = firstDead, e = end(bubulles); i != e; ++i)
			p->bubblePopped(**i);

	bubulles.erase(firstDead, end(bubulles));

	bubulles.insert(
		end(bubulles),
		make_move_iterator(begin(newBubulles)),
		make_move_iterator(end(newBubulles))
	);

	newBubulles.clear();

	for(ParticulePop &p : particles) {
		p.update();
		global<dq::parts>().enqueue(p.makeAttribs());
	}

	particles.erase(std::remove_if(
		begin(particles),
		end(particles),
		[](ParticulePop const &p) {
			return p.pos.y > M_HEIGHT + 1.0f;
		}
	), end(particles));
}

void GlobalState::display() {
	global<progs>().background.use();
	global<progs>().background.t = t * 0.001f;
	global<dq::bg>().display();

	for(auto const &b : bubulles)
		global<dq::bulles>().enqueue(b->makeAttribs());

	for(auto const &p : persos)
		global<dq::perso>().enqueue(p->makeAttribs());

	global<progs>().bulles.use();
	global<dq::bulles>().displayAndClear();

	global<progs>().perso.use();
	global<dq::perso>().displayAndClear();

	global<progs>().particles.use();
	global<dq::parts>().displayAndClear();

	{
		std::stringstream ss;
		ss << std::fixed << std::setprecision(2);
		ss << scores[0];
		scoreTxt[0].setText(ss.str());
		ss.str({});
		ss << scores[1];
		scoreTxt[2].setText(ss.str());
	}

	global<progs>().text.use();
	for(int i = 0; i < 3; ++i) {
		global<progs>().text.pvMatrix = global<camera>() * scoreTxt[i].transform();
		scoreTxt[i].display();
	}
}
