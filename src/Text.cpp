#include "Text.h"

#include <iostream>

#include <GL/glew.h>

#include "GlobalState.h"
#include "Programs.h"

Text::Text(std::string const &str)
	: _queue{global<unitVbo>(), global<progs>().text}
	, _length{str.length()} {
	for(unsigned char c : str)
		_queue.enqueue({GLuint(c)});
}

void Text::setText(std::string const &str) {
	_length = str.length();
	_queue.clear();
	for(char c : str)
		_queue.enqueue({GLuint(c)});
}

glm::mat3 Text::transform() const {
	glm::mat3 tm{1.0f};

	float angle = 0.0f;

	float a = glk::deg2rad(-angle);
	float s = std::sin(a);
	float c = std::cos(a);

	tm[0][0] = c * scale.x;
	tm[1][1] = c * scale.y;
	tm[0][1] = s * scale.x;
	tm[1][0] = -s * scale.y;

	glm::vec2 o = -alignment * scale;
	o.x *= _length * 0.5f;

	tm[2][0] = c * o.x - s * o.y + pos.x;
	tm[2][1] = s * o.x + c * o.y + pos.y;

	return tm;
}

void Text::display() {
	auto &prg = global<progs>().text;
	prg.use();
	TRY_GL(glActiveTexture(GL_TEXTURE4));
	TRY_GL(glBindTexture(GL_TEXTURE_2D, global<fontTex>()));
	prg.fontAtlas = 4;
	prg.color = color;
	_queue.display();
}
