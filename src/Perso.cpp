#include "Perso.h"
#include <algorithm>

#include "GlobalState.h"

extern std::mt19937 prng;

namespace {
	constexpr float
		VITESSE = 0.075f,
		MAX_VEL = 0.25f,
		SAUT = 0.1f,
		JMP_DECAY = 0.05,
		GRAVITY = 0.005f,
		LANDING_DAMP = 0.1f,
		JETPACK = 0.01f,
		JETPACK_TURN = 0.1f,
		JETPACK_PULSE = 0.25f,
		VEL_POP = 0.1f,
		BUBBLE_PULL = 0.001f;

	glm::vec2 const
		PERSO_SIZE(0.3f, 0.8f),
		JETPACK_TVEC(std::cos(JETPACK_TURN), std::sin(JETPACK_TURN));

	using dist = std::uniform_real_distribution<float>;
}


Perso::Perso(int num) : num{num} { }

void Perso::abullir(Bulle &b) {
	if (b.isAlive) {
		bulle = &b;
		b.vel += LANDING_DAMP / (b.radius * b.radius) * vel;
		cling();
	}
}

void Perso::cling() {
	vert = glm::normalize(pos - bulle->pos);
	pos = bulle->pos + bulle->radius * vert;
}

void Perso::avancer(int sens) {
	glm::vec2 rayon = glm::normalize(pos - bulle->pos);
	glm::vec2 ortho(-rayon.y, rayon.x);

	vel = sens * VITESSE * ortho;
	pos += vel;
}

void Perso::sauter() {
	vel = SAUT * vert;
	jmp = 0.0f;
	global<state>().scores[num] += 1.0f / bulle->radius;
	bulle->pop();
	bulle->vel -= LANDING_DAMP / glk::sqr(bulle->radius) * vel;
	bulle = nullptr;
}

void Perso::bubblePopped(Bulle const &b) {
	if(bulle == &b)
		sauter();
}

void Perso::update() {
	if (bulle) {
		vel = bulle->vel;
		pos += bulle->vel;
		bulle->vel.y += BUBBLE_PULL / glk::sqr(bulle->radius);

		if (global<input>().goRight(num))
			avancer(1);

		if (global<input>().goLeft(num))
			avancer(-1);


		if(!--bulle->time)
			global<state>().scores[num] += 2.0f / bulle->radius;

		if(!bulle->time || !bulle->isAlive || global<input>().jumpPressed(num))
			sauter();
		else
			cling();

	} else {

		if(global<input>().jumpPressed(num)) {
			vel += JETPACK_PULSE * vert;
			for(int i = 0; i < 20; ++i)
				global<state>().particles.emplace_back(
					pos,
					-0.1f * vert + glm::vec2(dist{-0.2f, 0.2f}(prng), dist{-0.2f, 0.2f}(prng)),
					glm::vec3(
						dist{0.9f, 1.0f}(prng),
						dist{0.9f, 1.0f}(prng),
						dist{0.9f, 1.0f}(prng)
					)
				);

		} else if(global<input>().jump(num)) {
			vel += JETPACK * vert;
			for(int i = 0; i < 4; ++i)
				global<state>().particles.emplace_back(
					pos,
				    -0.1f * vert + glm::vec2(dist{-0.05f, 0.05f}(prng), dist{-0.05f, 0.05f}(prng)),
				    glm::vec3(
					    dist{0.7f, 1.0f}(prng),
					    dist{0.7f, 1.0f}(prng),
					    dist{0.0f, 0.2f}(prng)
				    )
				);
		}

		if (global<input>().goRight(num))
			vert = glm::vec2(
				vert.x * JETPACK_TVEC.x - vert.y * JETPACK_TVEC.y,
				vert.y * JETPACK_TVEC.x + vert.x * JETPACK_TVEC.y
			);

		if (global<input>().goLeft(num))
			vert = glm::vec2(vert.x * JETPACK_TVEC.x + vert.y * JETPACK_TVEC.y,
				vert.y * JETPACK_TVEC.x - vert.x * JETPACK_TVEC.y
			);

		vel.y += jmp * GRAVITY;

		if (glm::dot(vel, vel) > glk::sqr(MAX_VEL))
			vel *= 0.95f;

		pos += vel;

		if (vel.x > 0.0f && pos.x > GlobalState::M_WIDTH)
			pos.x = 0.0f;
		if (vel.x < 0.0f && pos.x < 0.0f)
			pos.x = GlobalState::M_WIDTH;
		if (vel.y > 0.0f && pos.y > GlobalState::M_HEIGHT)
			pos.y = 0.0f;
		if (vel.y < 0.0f && pos.y < 0.0f)
			pos.y = GlobalState::M_HEIGHT;

		auto const &bubulles = global<state>().bubulles;
		glm::vec2 center = pos + vert * PERSO_SIZE.y / 2.0f;
		auto touche = std::find_if(
			begin(bubulles),
			end(bubulles),
			[&center](auto const &p) {
				glm::vec2 dist = p->pos - center;
				float radius = p->radius + PERSO_SIZE.y / 2.0f;
				return glm::dot(dist, dist) <= radius;
			}
		);

		if (global<input>().jump(num)) {
			jmp += JMP_DECAY;
			if (jmp > 1.0f)
				jmp = 1.0f;

			if(touche != end(bubulles) && glm::dot(vel, vel) >= glk::sqr(VEL_POP / (*touche)->radius)) {
				global<state>().scores[num] += 1.0f / (*touche)->radius;
				(*touche)->pop();
			}

		} else {
			jmp = 1.0f;

			if (touche != end(bubulles))
				abullir(**touche);
		}
	}
}

PersoAttribs Perso::makeAttribs() const {
	glm::mat3 tm;

	float s = vert.x;
	float c = -vert.y;

	glm::vec2 sc = PERSO_SIZE;

	tm[0][0] = c * sc.x;
	tm[1][1] = c * sc.y;
	tm[0][1] = s * sc.x;
	tm[1][0] = -s * sc.y;

	tm[2][0] = pos.x;
	tm[2][1] = pos.y;

	return {tm, num};
}
