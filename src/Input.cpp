#include "Input.h"

#include <SDL2/SDL_keyboard.h>

Input::Input()
: _keys{SDL_GetKeyboardState(nullptr)} { }


void Input::update() {
	pJump[0] = jump(0);
	pJump[1] = jump(1);
}

bool Input::jumpPressed(int player) const {
	return jump(player) && !pJump[player];
}

bool Input::goLeft(int player) const {
	return _keys[player ? SDL_SCANCODE_LEFT : SDL_SCANCODE_A];
}

bool Input::goRight(int player) const {
	return _keys[player ? SDL_SCANCODE_RIGHT : SDL_SCANCODE_D];
}

bool Input::jump(int player) const {
	return _keys[player ? SDL_SCANCODE_UP : SDL_SCANCODE_W];
}
