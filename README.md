<h1>Game Jam (Sujet Bulles) - Limonade</h1>

<p><h2>Description du travail</h2>
Jeu 2D à deux joueurs dans lequel il faut éclater le plus de bulles avec sa bouteille de limonade.

<h3>Commandes</h3>
<ul><li>ZQD pour le joueur 1
    <li>Flèche Haut/Bas/Droite pour le joueur 2
    <li>Droite/Gauche pour tourner autour d'une bulle ou en l'air
    <li>Haut pour accélérer avec le jetpack
    <li>Presser Haut pour sauter ou se propulser avec le jetpack
</ul></p>


<p><h2>Technologies et langages utilisés</h2>
<ul><li>CLion
    <li>C++
    <li>SDL2
    <li>OpenGL
    <li>Git
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Quentin Diaz
    <li>Ludovic Jozereau
</ul></p>

Dépendances
--
    glk (git@gitlab.com:D-z/glk.git)
     + Boost 1.59
     + GLM 0.9.7.1
     + Un compilateur qui ne bug pas trop (Clang 3.6.0 est OK)
    SDL2
    SDL2_image
    OpenGL
    GLM 0.9.7.1
    GLEW
    GLU

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>