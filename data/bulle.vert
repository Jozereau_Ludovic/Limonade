#version 330

uniform mat3 pvMatrix;

in vec2 vertexCoord;
in vec2 pos;
in vec2 size;
in float ttl;
in vec3 color;

out VSOut {
	vec2 uv;
	float ttl;
	vec3 col;
} _out;

void main() {
	_out.uv = vertexCoord - 0.5;
	_out.ttl = ttl;
	_out.col = color;
	gl_Position = vec4(pvMatrix * vec3(pos + _out.uv * size, 1.0), 1.0);
}
