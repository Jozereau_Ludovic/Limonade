#version 330

in vec2 vertexCoord;
in float unused;

out VSOut {
	vec2 uv;
} _out;

void main() {
	_out.uv = vertexCoord;
	gl_Position = vec4(2.0 * vertexCoord - 1.0, 0.0, 1.0);
}
