#version 330

uniform mat3 pvMatrix;

in vec2 vertexCoord;
in vec2 pos;
in vec3 color;

out VSOut {
	vec2 uv;
	vec3 col;
} _out;

const float size = 0.075;

void main() {
	_out.uv = vertexCoord - 0.5;
	_out.col = color;
	gl_Position = vec4(pvMatrix * vec3(pos + _out.uv * size, 1.0), 1.0);
}
