#version 330

uniform float t;

in VSOut {
	vec2 uv;
} _in;

out vec4 col;

const vec3 tCol = vec3(0.15, 0.25, 0.35), bCol = vec3(0.3, 0.4, 0.7);

void main() {
	float tt = _in.uv.y + 0.025 * sin(30.0 * _in.uv.x);

	const int bands = 15;

	tt -= t;

	tt *= bands;

	float i;
	float f = modf(tt, i);

	tt = (i + (1 - f)) / bands;

	tt += t;

	col = vec4(mix(bCol, tCol, tt), 1.0);
}
