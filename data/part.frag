#version 330

in VSOut {
	vec2 uv;
	vec3 col;
} _in;

out vec4 col;

void main() {
	float d = length(_in.uv);
	col = vec4(_in.col + 0.1, smoothstep(0.5, 0.45, d));
}
