#version 330

uniform mat3 pvMatrix;

in vec2 vertexCoord;
in mat3 transform;
in int pNum;

out VSOut {
	vec2 uv;
	flat int pNum;
} _out;

void main() {
	_out.uv = vertexCoord;
	_out.pNum = pNum;
	gl_Position = vec4(
		pvMatrix * transform * vec3(vertexCoord - vec2(0.5, 1.0), 1.0),
		1.0
	);
}
