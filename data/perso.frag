#version 330

in VSOut {
	vec2 uv;
	flat int pNum;
} _in;

out vec4 col;

const float neck = 0.3, aa = 0.02;
const vec3 labels[2] = vec3[](
	vec3(0.1, 0.5, 1.0),
	vec3(1.0, 0.5, 0.1)
);

void main() {
	float width = smoothstep(1.0, 0.4, _in.uv.y);

	float leftSide = 0.5 - neck * 0.5 - (0.5 - neck * 0.5) * width;
	float rightSide = 0.5 + neck * 0.5 + (0.5 - neck * 0.5) * width;

	float left = smoothstep(leftSide, leftSide + aa, _in.uv.x);
	float right = smoothstep(rightSide, rightSide - aa, _in.uv.x);

	float cx = _in.uv.x - 0.5;
	cx *= cx;
	cx *= cx;
	float bottom = smoothstep(cx - aa, cx, _in.uv.y);

	float shade = mix(0.7, 1.0, smoothstep(leftSide, rightSide, _in.uv.x));

	vec2 cd = _in.uv - vec2(0.5, 0.3);
	cd.y *= 2.0;

	vec3 color = (_in.uv.y > 0.2 && _in.uv.y < 0.4)
		? (dot(cd, cd) > 0.05 ? labels[_in.pNum] : vec3(1.0))
		: vec3(0.8, 0.95, 1.0);

	col = left * right * bottom * vec4(shade * color, 1.0);
}
