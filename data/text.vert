#version 330

uniform mat3 pvMatrix;

layout(location = 0) in vec2 vertexCoord;
layout(location = 1) in uint index;

out vec2 vUv;
flat out uint vIndex;

void main() {
	gl_Position = vec4(pvMatrix * vec3(vertexCoord + vec2(float(gl_InstanceID) * 8.0/16.0, 0.0), 1.0f), 1.0);
	vUv = vertexCoord;
	vIndex = index;
}
