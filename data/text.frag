#version 330

uniform sampler2D fontAtlas;
uniform vec4 color;

in vec2 vUv;
flat in uint vIndex;

out vec4 col;

vec2 charUv(uint index) {
	return vec2(
		float(index % 16u),
		float(index / 16u)
	);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
	col = color * texture(fontAtlas, (charUv(vIndex) + vUv) / 16.0, 0)/* * vec4(hsv2rgb(vec3(float(vIndex) * 0.07, 1.0, 1.0)), 1.0)*/;
}
