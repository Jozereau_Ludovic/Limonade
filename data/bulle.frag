#version 330

in VSOut {
	vec2 uv;
	float ttl;
	vec3 col;
} _in;

out vec4 col;

const float edge = 0.47;
const vec2 specPos = vec2(-0.2);

void main() {
	// Distance to center
	float d = length(_in.uv);
	const float edgeMid = (0.5 + edge) / 2.0;

	// Full shape of the bubble
	float outer = smoothstep(0.5, edgeMid, d);

	// Distance to the specular speck
	float sd = length(_in.uv - specPos);

	// Circle
	float stroke = smoothstep(edge, edgeMid, d);

	// Specular
	float spec = smoothstep(0.08, 0.06, sd);

	// Shading
	float shade = smoothstep(0.5, 1.0, sd);

	col = mix(
		vec4(_in.col + stroke * vec3(0.1), outer * (stroke + shade)),
		vec4(1.0, 1.0, 1.0, outer),
		spec
	);
}
